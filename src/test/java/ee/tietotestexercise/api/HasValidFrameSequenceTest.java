package ee.tietotestexercise.api;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class HasValidFrameSequenceTest {

  private static ValidationRule rule;
  private static List<String> errors;

  @BeforeAll
  static void setUp() {
    rule = new HasValidFrameSequence();
    errors = new ArrayList<>();
  }

  @Test
  void firstFrameNoErrors() {
    Frame first = createFrame(1);
    rule.validate(first, List.of(), errors);
    assertTrue(errors.isEmpty());
  }

  @Test
  void correctFrameSequenceValidationNoErrors() {
    Frame first = createFrame(1);
    Frame second = createFrame(2);
    rule.validate(second, List.of(first), errors);
    assertTrue(errors.isEmpty());
  }

  @Test
  void incorrectFrameSequenceValidationErrors() {
    Frame first = createFrame(2);
    Frame second = createFrame(1);
    rule.validate(second, List.of(first), errors);
    assertFalse(errors.isEmpty());
  }

  private Frame createFrame(int number) {
    return new Frame(number, List.of(0, 0));
  }
}