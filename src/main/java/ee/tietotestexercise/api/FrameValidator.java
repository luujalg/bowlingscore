package ee.tietotestexercise.api;

import java.util.ArrayList;
import java.util.List;

class FrameValidator {

  private final List<ValidationRule> validationRules;

  FrameValidator() {
    validationRules = List.of(
      new HasValidFrameNumber(),
      new HasValidFrameSequence(),
      new HasValidChances()
    );
  }

  void validate(Frame frame, List<Frame> frames) {
    List<String> errors = new ArrayList<>();
    validationRules.forEach(rule -> rule.validate(frame, frames, errors));
    if (!errors.isEmpty()) {
      throw new IllegalArgumentException(createErrorMessage(errors, frame));
    }
  }

  String createErrorMessage(List<String> errors, Frame frame) {
    return "Invalid frame received:\n" +
      frame + "\n" +
      "Validation errors:\n" +
      buildValidationErrorMessage(errors) + "\n";
  }

  private String buildValidationErrorMessage(List<String> errors) {
    StringBuilder sb = new StringBuilder();
    errors.forEach(error -> sb.append(error).append("\n"));
    return sb.toString();
  }

}
