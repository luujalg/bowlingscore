package ee.tietotestexercise.api;

import java.util.List;

class HasValidChances implements ValidationRule {

  private static final int MAX_CHANCES_ON_LAST_FRAME_IF_OPEN = 2;
  private static final int STRIKE_SPARE_VALUE = 10;
  private static final int MAX_CHANCES_ON_FINAL_FRAME_IF_NOT_OPEN = 3;
  private static final int MAX_SCORE_IN_LAST_FRAME = 30;

  @Override
  public void validate(Frame frame, List<Frame> frames, List<String> errors) {
    if (hasInvalidChancePoints(frame)) {
      errors.add(String.format("Invalid score. Valid points for each chance ranges from %d to %d",
        Frame.LOWEST_VALID_SCORE, Frame.MAX_POINTS_FROM_SINGLE_TURN));
      return;
    }

    if (frame.getNumber() < Frame.LAST_FRAME) {
      validate(frame, errors);
    } else {
      validateLastFrame(frame, errors);
    }
  }

  private boolean hasInvalidChancePoints(Frame frame) {
    return frame.getPoints().stream()
      .anyMatch(chance -> chance < Frame.LOWEST_VALID_SCORE || chance > STRIKE_SPARE_VALUE);
  }

  private void validate(Frame frame, List<String> errors) {
    int chances = frame.getPoints().size();
    int pointsSum = getSumOfPoints(frame);

    if (frame.getPoints().isEmpty()) {
      errors.add(String.format("Invalid number of chances: %d.", chances));
      return;
    }

    if (chances > Frame.CHANCES_PER_FRAME
      || chances == Frame.CHANCES_PER_FRAME_IF_STRIKE && pointsSum != STRIKE_SPARE_VALUE) {
      errors.add(String.format("Invalid number of chances. Maximum allowed chances per non-final frame: %d.",
        Frame.CHANCES_PER_FRAME));
    }

    if (pointsSum > Frame.MAX_POINTS_FROM_SINGLE_TURN) {
      errors.add(String.format("Invalid points sum. Maximum allowed points per non-final frame turn: %d",
        Frame.MAX_POINTS_FROM_SINGLE_TURN));
    }
  }

  private void validateLastFrame(Frame frame, List<String> errors) {
    boolean hasStrikesOrSpare = hasStrikesOrSpare(frame);
    int chances = frame.getPoints().size();
    if (hasStrikesOrSpare && chances != MAX_CHANCES_ON_FINAL_FRAME_IF_NOT_OPEN) {
      errors.add(String.format("Invalid number of chances. " +
        "If no open frame occurred on last frame, then the allowed number of chances is %d.",
        MAX_CHANCES_ON_FINAL_FRAME_IF_NOT_OPEN));
    }
    if (!hasStrikesOrSpare) {
      validateLastFrameWithOpenFrame(frame, errors);
    }
  }

  private void validateLastFrameWithOpenFrame(Frame frame, List<String> errors) {
    int chances = frame.getPoints().size();
    if (chances != MAX_CHANCES_ON_LAST_FRAME_IF_OPEN) {
      errors.add(String.format("Invalid number of chances. " +
          "If open frame occurred on last frame, then the allowed number of chances is %d.",
        MAX_CHANCES_ON_LAST_FRAME_IF_OPEN));
    }

    int sum = getSumOfPoints(frame);
    if (sum < Frame.LOWEST_VALID_SCORE || sum >= STRIKE_SPARE_VALUE) {
      errors.add(String.format("Invalid score. Valid score in an open frame ranges from %d to %d.",
        Frame.LOWEST_VALID_SCORE, Frame.MAX_POINTS_FROM_SINGLE_TURN - 1));
    }
  }

  private void validateLastFrameWithNonOpenFrame(Frame frame, List<String> errors) {
    int chances = frame.getPoints().size();
    if (chances != MAX_CHANCES_ON_FINAL_FRAME_IF_NOT_OPEN) {
      errors.add(String.format("Invalid number of chances. " +
        "If no open frame occurred on last frame, then the allowed number of chances is %d.",
        MAX_CHANCES_ON_FINAL_FRAME_IF_NOT_OPEN));
    }

    int sum = getSumOfPoints(frame);
    long strikes = getNumberOfStrikes(frame);
    long minAllowedFrameScore = (strikes > 0) ? strikes * STRIKE_SPARE_VALUE : STRIKE_SPARE_VALUE;
    if (sum < minAllowedFrameScore || sum > MAX_SCORE_IN_LAST_FRAME) {
      errors.add(String.format("Invalid score in non-open last frame. Valid score ranges from %d to %d.",
        minAllowedFrameScore, MAX_SCORE_IN_LAST_FRAME));
    }
  }

  private int getSumOfPoints(Frame frame) {
    return frame.getPoints().stream()
      .reduce(0, (sum, chance) -> sum += chance);
  }

  private int getSumOfPoints(Frame frame, int limit) {
    return frame.getPoints().stream()
      .limit(limit)
      .reduce(0, (sum, chance) -> sum += chance);
  }

  private long getNumberOfStrikes(Frame frame) {
    return frame.getPoints().stream()
      .filter(chance -> chance == STRIKE_SPARE_VALUE)
      .count();
  }

  private boolean hasStrikesOrSpare(Frame frame) {
    boolean hasStrikes = frame.getPoints().stream()
      .anyMatch(chance -> chance.equals(STRIKE_SPARE_VALUE));

    int pointsFromFirstTwoChances = getSumOfPoints(frame, 2);
    boolean hasSpare = pointsFromFirstTwoChances == STRIKE_SPARE_VALUE;

    return hasStrikes || hasSpare;
  }

}
