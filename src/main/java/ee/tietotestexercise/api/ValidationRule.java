package ee.tietotestexercise.api;

import java.util.List;

interface ValidationRule {
  void validate(Frame frame, List<Frame> frames, List<String> errors);
}
