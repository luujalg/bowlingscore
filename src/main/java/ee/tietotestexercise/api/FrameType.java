package ee.tietotestexercise.api;

enum FrameType {
  OPEN(0),
  SPARE(1),
  STRIKE(2);

  int chancesToAddToFrameScore;

  FrameType(int chancesToAddToFrameScore) {
    this.chancesToAddToFrameScore = chancesToAddToFrameScore;
  }
}
