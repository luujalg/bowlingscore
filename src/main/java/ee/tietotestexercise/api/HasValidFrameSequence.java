package ee.tietotestexercise.api;

import java.util.List;

class HasValidFrameSequence implements ValidationRule {

  @Override
  public void validate(Frame frame, List<Frame> frames, List<String> errors) {
    if (frame.getNumber() == Frame.FIRST_FRAME && frames.isEmpty()) {
      return;
    }

    Frame lastFrame = frames.get(frames.size() - 1);
    if (lastFrame.getNumber() + 1 != frame.getNumber()) {
      errors.add(String.format("Invalid sequence. Last frame is #%d, new frame is #%d.",
        lastFrame.getNumber(), frame.getNumber()));
    }
  }
}
