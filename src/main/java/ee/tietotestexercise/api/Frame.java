package ee.tietotestexercise.api;

import java.util.List;

public class Frame {

  static final int MAX_POINTS_FROM_SINGLE_TURN = 10;
  static final int CHANCES_PER_FRAME = 2;
  static final int LOWEST_VALID_SCORE = 0;
  static final int FIRST_FRAME = 1;
  static final int LAST_FRAME = 10;
  static final int CHANCES_PER_FRAME_IF_STRIKE = 1;

  private int number;
  private List<Integer> points;
  private int score;
  private FrameType type;

  public Frame(int number, List<Integer> points) {
    this.number = number;
    this.points = points;
    setScore();
    setType();
  }

  void updateScore(int score) {
    this.score = score;
  }

  public int getNumber() {
    return number;
  }

  public int getScore() {
    return score;
  }

  FrameType getType() {
    return type;
  }

  public List<Integer> getPoints() {
    return points;
  }

  @Override
  public String toString() {
    String points = this.points.stream()
      .map(Object::toString)
      .reduce("", (sub, element) -> sub + "," + element);
    return String.format("FRAME:\nNR: %d, POINTS: %s, SCORE: %d", number, points, score);
  }

  private void setScore() {
    score = points.stream()
      .reduce(0, (sum, value) -> sum +=value);
  }

  private void setType() {
    if (score < MAX_POINTS_FROM_SINGLE_TURN) {
      type = FrameType.OPEN;
    }
    if (points.size() == CHANCES_PER_FRAME) {
      type = FrameType.SPARE;
    } else {
      type = FrameType.STRIKE;
    }
  }

}
