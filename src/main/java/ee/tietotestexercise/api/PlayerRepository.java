package ee.tietotestexercise.api;

import java.util.ArrayList;
import java.util.List;

class PlayerRepository {

  private final List<Player> players;

  PlayerRepository() {
    players = new ArrayList<>();
  }

  void add(Player player) {
    players.add(player);
  }

  void addAll(List<Player> players) {
    this.players.addAll(players);
  }

  void remove(int id) {
    Player player = find(id);
    players.remove(player);
  }

  void removeAll() {
    players.clear();
  }

  Player find(int id) {
    return players.stream()
      .filter(player -> player.getId() == id)
      .findFirst()
      .orElse(null);
  }
}
