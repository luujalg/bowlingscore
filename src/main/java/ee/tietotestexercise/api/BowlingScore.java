package ee.tietotestexercise.api;

import java.util.List;
import java.util.Map;

import static java.util.Collections.emptyList;

/**
 * BowlingScore API.
 * A simple implementation of a bowling game. Allows adding and removing players. Once players are registered, valid
 * frames can be added. The current game can be reset (added players remain, but scores are reset) or quit entirely
 * (players are deleted along with their scores). The state of the game is accessible by querying a players frames,
 * scores for every player or frames for every player.
 */
public class BowlingScore {

  private final PlayerRepository playerRepository;
  private final FrameRepository frameRepository;

  public BowlingScore() {
    playerRepository = new PlayerRepository();
    frameRepository = new FrameRepository();
  }

  /**
   * Register player by adding them to the player repository. Initialise player frame collection in frame repository.
   * @param player Player to be registered
   */
  public void addPlayer(Player player) {
    playerRepository.add(player);
    frameRepository.initPlayerFrames(player.getId());
  }

  /**
   * Remove player from player repository. Remove player frames from frame repository.
   * @param playerId Player ID
   */
  public void removePlayer(int playerId) {
    playerRepository.remove(playerId);
    frameRepository.removePlayerFrames(playerId);
  }

  /**
   * Remove every player from player repository. Remove frames for every player from frame repository.
   */
  public void removeAllPlayers() {
    playerRepository.removeAll();
    frameRepository.removeAllFrames();
  }

  /**
   * Register a new frame for a player. If valid, adds frame to player frame collection in the frame repository.
   * @param frame Frame to be registered
   * @param playerId Player ID
   */
  public void addFrame(Frame frame, int playerId) {
    if (playerRepository.find(playerId) != null) {
      frameRepository.addFrame(frame, playerId);
    }
  }

  /**
   * Get all registered frames for player.
   * @param playerId Player ID
   * @return Collection of player's frames. Empty list if player not found.
   */
  public List<Frame> getPlayerFrames(int playerId) {
    if (playerRepository.find(playerId) != null) {
      return frameRepository.getFrames(playerId);
    }

    return emptyList();
  }

  /**
   * Get every registered frame for every player in the current game.
   * @return A map of frames by player ID
   */
  public Map<Integer, List<Frame>> getAllFrames() {
    return frameRepository.getAllFrames();
  }

  /**
   * Get scores for every player.
   * @return A map of scores by player ID.
   */
  public Map<Integer, Integer> getPlayerScores() {
    return frameRepository.getScores();
  }

  /**
   * Reset player frames. Retains previously added players, but empties their frame collections.
   */
  public void resetFrames() {
    frameRepository.reset();
  }

}
