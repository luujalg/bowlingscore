package ee.tietotestexercise.demo.player;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@AllArgsConstructor
@Builder
public class PlayerDto {

  private int id;
  private String name;

}
