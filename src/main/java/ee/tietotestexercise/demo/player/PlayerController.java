package ee.tietotestexercise.demo.player;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RequiredArgsConstructor
@RestController
@RequestMapping("/players")
public class PlayerController {

  private final PlayerService playerService;

  @PostMapping
  public PlayerDto addPlayer(@RequestParam String name) {
    return playerService.addPlayer(name);
  }

  @DeleteMapping
  public void deletePlayer(@RequestParam int id) {
    playerService.removePlayer(id);
  }

  @DeleteMapping("/all")
  public void deleteAllPlayers() {
    playerService.removeAllPlayers();
  }
}
