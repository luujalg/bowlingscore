package ee.tietotestexercise.demo.player;

import ee.tietotestexercise.api.Player;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.WARN)
public interface PlayerConverter {

  PlayerDto convert(Player player);
}
