package ee.tietotestexercise.demo.game;

import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class ScoreDto {

  private final int playerId;
  private final int score;

}
