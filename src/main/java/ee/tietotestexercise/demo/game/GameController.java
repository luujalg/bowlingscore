package ee.tietotestexercise.demo.game;

import ee.tietotestexercise.demo.frame.FrameService;
import ee.tietotestexercise.demo.player.PlayerService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequiredArgsConstructor
@RestController
@RequestMapping("/game")
public class GameController {

  private final PlayerService playerService;
  private final FrameService frameService;

  @GetMapping("/reset")
  public void resetFrames() {
    frameService.resetFrames();
  }

  @GetMapping("/quit")
  public void quitGame() {
    playerService.removeAllPlayers();
  }

  @GetMapping("/score")
  public List<ScoreDto> getPlayerScores() {
    return frameService.getPlayerScores();
  }
}
