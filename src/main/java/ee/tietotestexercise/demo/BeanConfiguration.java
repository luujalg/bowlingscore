package ee.tietotestexercise.demo;

import ee.tietotestexercise.api.BowlingScore;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BeanConfiguration {

  @Bean
  public BowlingScore bowlingScore() {
    return new BowlingScore();
  }

}
