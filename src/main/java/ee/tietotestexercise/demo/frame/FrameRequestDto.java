package ee.tietotestexercise.demo.frame;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class FrameRequestDto {

  private int playerId;
  private FrameDto frame;

}
