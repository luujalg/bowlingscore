package ee.tietotestexercise.demo.frame;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequiredArgsConstructor
@RestController
@RequestMapping("/frames")
public class FrameController {

  private final FrameService frameService;

  @PostMapping()
  public void addFrame(@RequestBody FrameRequestDto frameDto) {
    frameService.addFrame(frameDto);
  }

  @GetMapping("/{playerId}")
  public FrameResponseDto getPlayerFrames(@PathVariable int playerId) {
    return frameService.getPlayerFrames(playerId);
  }

  @GetMapping
  public List<FrameResponseDto> getAllFrames() {
    return frameService.getAllFrames();
  }

}
