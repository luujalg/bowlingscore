package ee.tietotestexercise.demo.frame;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class FrameResponseDto {

  private int playerId;
  private List<FrameDto> frames;

}
