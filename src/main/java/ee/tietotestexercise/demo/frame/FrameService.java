package ee.tietotestexercise.demo.frame;

import ee.tietotestexercise.api.BowlingScore;
import ee.tietotestexercise.api.Frame;
import ee.tietotestexercise.demo.game.ScoreDto;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@AllArgsConstructor
@Service
public class FrameService {

  private final BowlingScore bowlingScore;
  private final FrameConverter converter;

  public void addFrame(FrameRequestDto frameDto) {
    bowlingScore.addFrame(converter.convert(frameDto.getFrame()), frameDto.getPlayerId());
  }

  public FrameResponseDto getPlayerFrames(int playerId) {
    List<FrameDto> frames = converter.convert(bowlingScore.getPlayerFrames(playerId));
    return FrameResponseDto.builder()
      .playerId(playerId)
      .frames(frames)
      .build();
  }

  public List<FrameResponseDto> getAllFrames() {
    Map<Integer, List<Frame>> framesByPlayerId = bowlingScore.getAllFrames();
    return framesByPlayerId.keySet().stream()
      .map(key -> FrameResponseDto.builder()
        .playerId(key)
        .frames(converter.convert(framesByPlayerId.get(key)))
        .build())
      .sorted(Comparator.comparingInt(FrameResponseDto::getPlayerId))
      .collect(Collectors.toList());
  }

  public void resetFrames() {
    bowlingScore.resetFrames();
  }

  public List<ScoreDto> getPlayerScores() {
    Map<Integer, Integer> scores = bowlingScore.getPlayerScores();
    return scores.keySet().stream()
      .map(key -> new ScoreDto(key, scores.get(key)))
      .collect(Collectors.toList());
  }
}
